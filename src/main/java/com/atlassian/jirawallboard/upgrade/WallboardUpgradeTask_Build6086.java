package com.atlassian.jirawallboard.upgrade;

import com.atlassian.jira.ofbiz.DatabaseIterable;
import com.atlassian.jira.ofbiz.FieldMap;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.ofbiz.OfBizListIterator;
import com.atlassian.jira.util.Consumer;
import com.atlassian.jira.util.Resolver;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.EntityConditionList;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WallboardUpgradeTask_Build6086 implements PluginUpgradeTask
{
    private static final Logger log = Logger.getLogger(WallboardUpgradeTask_Build6086.class);

    private static final String TWITTER_GADGET_XML = "rest/gadgets/1.0/g/com.atlassian.jirawallboard.atlassian-wallboard-plugin:twitter-gadget/gadgets/twitterGadget.xml";

    private static final String PORTLET_CONFIGURATION_GADGET_XML_FIELD = "gadgetXml";

    private static final String PORTLET_CONFIGURATION_ENTITY_NAME = "PortletConfiguration";

    private static final String GADGET_USER_PREFERENCE_ENTITY_NAME = "GadgetUserPreference";

    private static final String GADGET_USER_PREFERENCE_PORTLET_CONFIGURATION_FIELD = "portletconfiguration";

    private static final String PORTLET_CONFIGURATION_ID_FIELD = "id";

    private final OfBizDelegator ofBizDelegator;

    public WallboardUpgradeTask_Build6086(final OfBizDelegator ofBizDelegator)
    {
        this.ofBizDelegator = ofBizDelegator;
    }

    public int getBuildNumber()
    {
        return 6086; // the last upgrade task for this plugin had build number 6085, so this is necessary
    }

    public String getShortDescription()
    {
        return "Removes broken twitter gadget from all dashboards that contain it";
    }

    public Collection<Message> doUpgrade() throws Exception
    {
        Level oldLogLevel = log.getLevel();
        log.setLevel(Level.INFO);

        log.info(getShortDescription());

        log.info("removing twitter gadget user preference entries from gadgetuserpreference table if any entries exist");

        // get a list of portletconfiguration ids that relate to the twitter gadget so that we can remove
        // the corresponding entries from the gadgetuserconfiguration table
        List<Long> portletConfigIds = loadTwitterGadgetPortletConfigIds();

        int numRowsAffectedByFirstRemove = 0;
        for (Long id : portletConfigIds)
        {
            // use a loop so that we don't kill the db server accidentally by passing it the whole list of
            // ids when the list is very long
            numRowsAffectedByFirstRemove += ofBizDelegator.removeByAnd(GADGET_USER_PREFERENCE_ENTITY_NAME,
                    new FieldMap(GADGET_USER_PREFERENCE_PORTLET_CONFIGURATION_FIELD, id));
        }

        log.info(numRowsAffectedByFirstRemove +
                " twitter gadget user preference instance(s) removed from gadgetuserpreference table");


        log.info("removing twitter gadget entries from portletconfiguration table if any entries exist");

        // delete twitter gadget entries from the portlet-configuration db table if they exist
        int numRowsAffectedBySecondRemove = 0;
        if (!portletConfigIds.isEmpty())
        {
            // the number of rows affected will be equal to the number of
            // times the twitter gadget appears in this jira instance, which can include multiple instances of the
            // gadget per user dashboard
            numRowsAffectedBySecondRemove = ofBizDelegator.removeByCondition(PORTLET_CONFIGURATION_ENTITY_NAME,
                    new EntityExpr(PORTLET_CONFIGURATION_GADGET_XML_FIELD, EntityOperator.LIKE, TWITTER_GADGET_XML));
        }

        log.info(numRowsAffectedBySecondRemove +
                " instance(s) of the twitter gadget were removed from portletconfiguration table");

        log.setLevel(oldLogLevel);

        return null;
    }

    public String getPluginKey()
    {
        return "com.atlassian.jirawallboard.atlassian-wallboard-plugin";
    }


    private List<Long> loadTwitterGadgetPortletConfigIds()
    {
        final Set<Long> portletConfigIds = new HashSet<Long>();

        int size = -1;
        DatabaseIterable<GenericValue> iterator = new DatabaseIterable<GenericValue>(size, new NoOpResolver<GenericValue>())
        {
            @Override
            protected OfBizListIterator createListIterator()
            {
                List<EntityExpr> conditionList = new ArrayList<EntityExpr>();
                conditionList.add(new EntityExpr(PORTLET_CONFIGURATION_GADGET_XML_FIELD,
                        EntityOperator.LIKE, TWITTER_GADGET_XML));

                EntityConditionList conditions = new EntityConditionList(conditionList, EntityOperator.AND);
                return ofBizDelegator.findListIteratorByCondition(PORTLET_CONFIGURATION_ENTITY_NAME, conditions);
            }
        };

        // consumes the output of the iterator
        Consumer<GenericValue> consumer = new Consumer<GenericValue>()
        {
            @Override
            public void consume(GenericValue v)
            {
                Long issueId = v.getLong(PORTLET_CONFIGURATION_ID_FIELD);
                if (issueId != null)
                {
                    portletConfigIds.add(issueId);
                }
            }
        };

        // do the actual fetching
        iterator.foreach(consumer);

        // all done
        log.info("Found " + portletConfigIds.size() + " unique portletconfiguration id(s) for the twitter gadget");

        return new ArrayList(portletConfigIds);
    }

    private static class NoOpResolver<E> implements Resolver<E, E>
    {
        @Override
        public E apply(E e) {
            return e;
        }
    }
}
