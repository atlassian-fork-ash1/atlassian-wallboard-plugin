package com.atlassian.jirawallboard.servlet;

import com.atlassian.templaterenderer.annotations.HtmlSafe;

import java.util.ArrayList;
import java.util.List;

public class MultiWallboardParams
{
    //Metadata to get resources and other features with
    private WallframeParams.WallboardMetadata wallboardMetadata;
    //List of urls of wallboards to cycle between
    private List<WallframeParams> wallframes;
    //Periodicity of cycling (in ms)
    private int cyclePeriod;
    //FX used for transition
    private String transitionFx;
    //Random order
    private boolean random;
    //String representing resources to require in the head
    private String resourceIncludes;

    private String title;

    public MultiWallboardParams()
    {
        wallboardMetadata = null;
        wallframes = new ArrayList<WallframeParams>();
        cyclePeriod = 10000;
        transitionFx = "fade";
        random = false;
        resourceIncludes = "";
        title = "";
    }

    public WallframeParams.WallboardMetadata getWallboardMetadata()
    {
        return wallboardMetadata;
    }

    public void setWallboardMetadata(WallframeParams.WallboardMetadata metadata)
    {
        this.wallboardMetadata = metadata;
    }

    public List<WallframeParams> getWallframes()
    {
        return wallframes;
    }

    public void appendWallframe(WallframeParams wallframe)
    {
        this.wallframes.add(wallframe);
    }

    public int getCyclePeriod()
    {
        return cyclePeriod;
    }

    public void setCyclePeriod(int cyclePeriod)
    {
        this.cyclePeriod = cyclePeriod;
    }

    public String getTransitionFx()
    {
        return transitionFx;
    }

    public void setTransitionFx(String transitionFx)
    {
        this.transitionFx = transitionFx;
    }

    public boolean isRandom()
    {
        return random;
    }

    public void setRandom(boolean random)
    {
        this.random = random;
    }

    @HtmlSafe
    public String getResourceIncludes()
    {
        return resourceIncludes;
    }

    public void setResourceIncludes(String resourceIncludes)
    {
        this.resourceIncludes = resourceIncludes;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }
}
