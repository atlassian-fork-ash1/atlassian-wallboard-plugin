package com.atlassian.jirawallboard.servlet;

import com.atlassian.gadgets.dashboard.DashboardId;
import com.atlassian.gadgets.dashboard.DashboardService;
import com.atlassian.gadgets.dashboard.DashboardState;
import com.atlassian.gadgets.dashboard.PermissionException;
import com.atlassian.jira.favourites.FavouritesManager;
import com.atlassian.jira.portal.PortalPage;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ConfigureWallboardDialog extends ProjectActionSupport
{

    private final PluginSettingsFactory psf;
    private final FavouritesManager<PortalPage> favouritesManager;
    private final DashboardService dashboardService;
    private List<DashboardIdEntry> userFavourites;

    public ConfigureWallboardDialog(PluginSettingsFactory psf, FavouritesManager<PortalPage> favouritesManager, DashboardService dashboardService)
    {
        this.psf = psf;
        this.favouritesManager = favouritesManager;
        this.dashboardService = dashboardService;
    }

    @Override
    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @Override
    @RequiresXsrfCheck
    public String doExecute() throws Exception
    {
        List<String> wallboardIds = new ArrayList<String>();
        final List<DashboardIdEntry> userFavourites = getUserFavourites();
        String[] dashboards = request.getParameterValues("dashboardId") == null ? new String[0] : request.getParameterValues("dashboardId");

        //For each parameter, if the key is a long, a favourite dashboardId and the value is on then put in userFavourites
        for (String dashboardId : dashboards)
        {
            Long id;
            try
            {
                id = Long.parseLong(dashboardId);
            }
            catch (NumberFormatException e)
            {
                continue;
            }
            for (DashboardIdEntry userFavourite : userFavourites)
            {
                if (userFavourite.getDashboardId().equals(id))
                {
                    wallboardIds.add(id.toString());
                }
            }
        }
        WallboardPluginSettings wps = new WallboardPluginSettings(psf, getLoggedInApplicationUser());
        wps.setDashboardIds(wallboardIds);

        // parse the cycle period and set value
        Integer cyclePeriod = WallboardServlet.CYCLE_PERIOD.getDefaultValue();
        try
        {
            String cyclePeriodString = request.getParameter(WallboardServlet.CYCLE_PERIOD.getKey());
            if(cyclePeriod != null)
            {
                cyclePeriod = Integer.parseInt(cyclePeriodString);
            }
        }
        catch (NumberFormatException e)
        {
            // silently consume the NFE, as the value will be set to the default in this case
        }
        wps.setCyclePeriod(cyclePeriod);

        //set the transition, falling back to default value if null
        String transitionFx = request.getParameter(WallboardServlet.TRANSITION_FX.getKey());
        wps.setTransitionFx(transitionFx != null ? transitionFx : WallboardServlet.TRANSITION_FX.getDefaultValue());

        //set the random flag (no need for default check - parseBoolean defaults to false for everything not resembling true
        wps.setRandom(Boolean.parseBoolean(request.getParameter(WallboardServlet.RANDOM.getKey())));
        wps.saveChanges();
        return returnComplete();
    }

    public List<DashboardIdEntry> getUserFavourites()
    {
        if (userFavourites == null)
        {
            ApplicationUser loggedInUser = getLoggedInApplicationUser();
            if (loggedInUser == null)
            {
                return Collections.emptyList();
            }

            WallboardPluginSettings wps = WallboardPluginSettings.loadSettings(psf, loggedInUser);
            Collection<Long> longs = favouritesManager.getFavouriteIds(loggedInUser, PortalPage.ENTITY_TYPE);
            userFavourites = new ArrayList<DashboardIdEntry>(longs.size());
            for (Long dashboardId : longs)
            {
                try
                {
                    DashboardState dashboardState = dashboardService.get(DashboardId.valueOf(dashboardId.toString()), getLoggedInApplicationUser().getName());
                    userFavourites.add(new DashboardIdEntry(dashboardId,
                            dashboardState.getTitle(),
                            wps.getDashboardIds().contains(dashboardId.toString())));
                }
                catch (PermissionException e)
                {
                    // skip, as user no longer has permission to access this favourite
                }
            }
        }
        return new ArrayList<DashboardIdEntry>(userFavourites);
    }

    public WallboardPluginSettings getWallboardPluginSettings()
    {
        return WallboardPluginSettings.loadSettings(psf, getLoggedInApplicationUser());
    }

    public class DashboardIdEntry
    {
        private final Long dashboardId;
        private final String title;
        private final boolean isSet;

        public DashboardIdEntry(Long dashboardId, String title, boolean set)
        {
            this.dashboardId = dashboardId;
            this.title = title;
            isSet = set;
        }

        public Long getDashboardId()
        {
            return dashboardId;
        }

        public String getTitle()
        {
            return title;
        }

        public boolean isSet()
        {
            return isSet;
        }
    }
}
