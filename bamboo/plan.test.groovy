plan(key:'TESTSTABLE', name:'Wallboards Stable - Functional and Unit Tests', description:' Functional and Unit tests for Wallboards Plugin Stable') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repositoryPLUGIN(repoName:'Wallboards plugin stable')
    repositoryJIRA(repoName:'JIRA 6.4.x branch')
    branches(pattern:'issue/.*')

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        functionalTests(pluginRepo:'Wallboards plugin stable', jiraRepo:'JIRA 6.4.x branch', jdkVersion: '1.7')
        jUnitTests(pluginRepo:'Wallboards plugin stable', jiraRepo:'JIRA 6.4.x branch', jdkVersion: '1.7')
    }
}

plan(key:'TESTDEFAULT', name:'Wallboards Default - Functional and Unit Tests', description:' Functional and Unit tests for Wallboards Plugin Default') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repositoryPLUGIN(repoName:'Wallboards plugin stable')
    repositoryJIRA(repoName:'JIRA master stash')
    branches(pattern:'issue/.*')

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        functionalTests(pluginRepo:'Wallboards plugin stable', jiraRepo:'JIRA master stash', jdkVersion: '1.8')
        jUnitTests(pluginRepo:'Wallboards plugin stable', jiraRepo:'JIRA master stash', jdkVersion: '1.8')
    }
}

plan(key:'TESTBREAKIT', name:'Wallboards Breakit - Functional and Unit Tests', description:' Functional and Unit tests for Wallboards Plugin Default') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repositoryPLUGIN(repoName:'Wallboards plugin default')
    repositoryJIRA(repoName:'JIRA 7.0 breakit branch')
    branches(pattern:'breakit/.*')

    stage(name:'Default stage', description: 'Default Stage', manual: 'false'){
        functionalTests(pluginRepo:'Wallboards plugin default', jiraRepo:'JIRA 7.0 breakit branch', jdkVersion: '1.8')
        jUnitTests(pluginRepo:'Wallboards plugin default', jiraRepo:'JIRA 7.0 breakit branch', jdkVersion: '1.8')
    }
}