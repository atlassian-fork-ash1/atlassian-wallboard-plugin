plan(key:'RELEASESTABLE', name:'Wallboards Stable - Release', description:'Release build for Wallboards Stable') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repository(name:'Wallboards plugin stable')

    stage(name:"Default stage", description:'Default stage', manual: 'true'){
        releaseTask(pluginRepo:'Wallboards plugin stable', jiraRepo:'JIRA 6.3.x branch', jdkVersion: '1.6')
    }
}

plan(key:'RELEASEDEFAULT', name:'Wallboards Default - Release', description:'Release build for Wallboards Default') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repository(name:'Wallboards plugin stable')

    stage(name:"Default stage", description:'Default stage', manual: 'true'){
        releaseTask(pluginRepo:'Wallboards plugin default', jiraRepo:'JIRA master stash', jdkVersion: '1.6')
    }
}

plan(key:'RELEASEBREAKIT', name:'Wallboards Breakit - Release', description:'Release build for Wallboards Breakit') {
    wallboardsLabels()
    wallboardsNotifications()
    wallboardsProject()

    repository(name:'Wallboards plugin default')

    stage(name:"Default stage", description:'Default stage', manual: 'true'){
        releaseTask(pluginRepo:'Wallboards plugin default', jiraRepo:'JIRA 7.0 breakit branch', jdkVersion: '1.8')
    }
}
